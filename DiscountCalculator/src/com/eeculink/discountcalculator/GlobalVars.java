package com.eeculink.discountcalculator;

import android.app.Application;

public class GlobalVars extends Application{
	
	
	

	    private static int seekBarPrice;

	    public static int getSeekBarPrice() {
	        return seekBarPrice;
	    }

	    public static void setSeekBarPrice(int seekBarPrice) {
	    	GlobalVars.seekBarPrice = seekBarPrice;
	    }
	    
	    private static float seekBarDiscount;

	    public static float getSeekBarDiscount() {
	        return seekBarDiscount;
	    }

	    public static void setSeekBarDiscount(float discountFloat) {
	    	GlobalVars.seekBarDiscount = discountFloat;
	    }
	    
	    
	    

	}
	
	


