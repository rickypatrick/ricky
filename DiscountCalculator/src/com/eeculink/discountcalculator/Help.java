package com.eeculink.discountcalculator;

import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;
import android.os.Bundle;

public class Help extends Activity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		setContentView(R.layout.help);
		
	}
	
	@Override
	public void onStart() {
	  super.onStart();
	 
	  EasyTracker.getInstance(this).activityStart(this);  // Google Analytics.
	}

		
		@Override
	    protected void onStop(){
	       super.onStop();

	       EasyTracker.getInstance(this).activityStop(this);  // Google Analytics

	    }
	
}
