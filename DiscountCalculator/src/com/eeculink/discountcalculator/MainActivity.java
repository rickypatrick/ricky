package com.eeculink.discountcalculator;

import java.text.DecimalFormat;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.android.vending.billing.IInAppBillingService;
import com.google.ads.*;
import com.google.analytics.tracking.android.EasyTracker;



public class MainActivity extends Activity {
	private AdView adView;
//	IInAppBillingService mService;

//	ServiceConnection mServiceConn = new ServiceConnection() {
//	   @Override
//	   public void onServiceDisconnected(ComponentName name) {
//	       mService = null;
//	   }

//	   @Override
//	   public void onServiceConnected(ComponentName name, 
//	      IBinder service) {
//	       mService = IInAppBillingService.Stub.asInterface(service);
//	   }
//	};
	
	
	
	EditText priceEditText;
	SeekBar priceSeekBar;
	EditText discountEditText;
	SeekBar discountSeekBar;
	TextView totalTextView;
	SeekBar discountSeekBar2;
	EditText discountEditText2;
	TextView savedTextView;
	
	TextView testPrefLoad;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		
		setContentView(R.layout.activity_main);
		
		SharedPreferences MYPREFS = getPreferences(0);
		String maxPrice = MYPREFS.getString("pref_maxprice", "100");
		String tax = MYPREFS.getString("pref_tax", "0");
		
		priceEditText = (EditText)findViewById(R.id.priceEditText);
		priceSeekBar = (SeekBar)findViewById(R.id.priceSeekBar);
		
		 
			
		
		
		
		priceSeekBar.setOnSeekBarChangeListener(priceSeekBarListener);
				
				
				
//		priceEditText.addTextChangedListener(priceTextListener);
	
		
		discountEditText = (EditText)findViewById(R.id.discountEditText);
		discountSeekBar = (SeekBar)findViewById(R.id.discountSeekBar);
		discountSeekBar.setOnSeekBarChangeListener(discountSeekBarListener);

		discountEditText2 = (EditText)findViewById(R.id.discountEditText2);
		discountSeekBar2 = (SeekBar)findViewById(R.id.discountSeekBar2);
		discountSeekBar2.setOnSeekBarChangeListener(discountSeekBarListener2);
	
		
		totalTextView = (TextView)findViewById(R.id.totalTextView);
		savedTextView = (TextView)findViewById(R.id.savedTextView);
		testPrefLoad = (TextView)findViewById(R.id.testPrefLoad);
//		BILLING CODE
//		bindService(new 
//		        Intent("com.android.vending.billing.InAppBillingService.BIND"),
//		                mServiceConn, Context.BIND_AUTO_CREATE);

		
		loadPrefs();
		
		
		
	}
	
	@Override
	  public void onStart() {
	    super.onStart();
	    // The rest of your onStart() code.
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	@Override
	  public void onStop() {
	    super.onStop();
	     // The rest of your onStop() code.
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }

	@Override
    protected void onResume(){
        super.onResume();
        // Set up a listener whenever a key changes
        loadPrefs();
    }
//	@Override
//	public void onDestroy() {
//	    super.onDestroy();
//	    if (mServiceConn != null) {
//	        unbindService(mServiceConn);
//	    }   
//	}

	private void loadPrefs() {
		// TODO Auto-generated method stub
		
		SharedPreferences MYPREFS = PreferenceManager.getDefaultSharedPreferences(this);
		String maxPrice = MYPREFS.getString("maxPrice", "100");
		String tax = MYPREFS.getString("tax", "0");
		try {
		int maxPriceInt = Integer.parseInt(maxPrice);
		
		
		priceSeekBar.setMax(maxPriceInt);
		} catch(NumberFormatException nfe) {
			   Log.e("MyApp", nfe.getMessage(), nfe);
			}
		

		
	}

	public OnSeekBarChangeListener priceSeekBarListener = new OnSeekBarChangeListener() {

		@Override
		public void onProgressChanged(SeekBar seekBar, int seekBarPrice,
				boolean fromUser) {
			
			
			GlobalVars.setSeekBarPrice(seekBarPrice);
			
			priceEditText.setText("" + "" + seekBarPrice);

			
			
			
			
		calculateTotal();
			
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			
			
		}
		
		
		
		
		
		
		
	};
	
	public OnSeekBarChangeListener discountSeekBarListener = new OnSeekBarChangeListener() {

		@Override
		public void onProgressChanged(SeekBar seekBar, int discountSeekBarProgress,
				boolean fromUser) {
//			double discountTotal = discountSeekBarProgress * .01;
//			float discountFloat = ((float) discountSeekBarProgress / 100);
//			GlobalVars.setSeekBarDiscount(discountFloat);
			
			discountEditText.setText("" + discountSeekBarProgress + "");
			
			calculateTotal();
			
		}

		

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			
		}
		
		
		

		
	};
	public OnSeekBarChangeListener discountSeekBarListener2 = new OnSeekBarChangeListener() {

		@Override
		public void onProgressChanged(SeekBar seekBar, int discount2SeekBarProgress,
				boolean fromUser) {
//			double discountTotal = discountSeekBarProgress * .01;
//			float discountFloat = ((float) discountSeekBarProgress / 100);
//			GlobalVars.setSeekBarDiscount(discountFloat);
			
			discountEditText2.setText("" + discount2SeekBarProgress + "");
			
			calculateTotal();
			
		}

		

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			// TODO Auto-generated method stub
			
		}
		
		
		

		
	};
	

	


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);
	    return super.onCreateOptionsMenu(menu);

	   
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    
	    switch (item.getItemId()) {
		case R.id.action_settings:
			startActivity(new Intent(this, Prefs.class));
			break;
		case R.id.help_settings:
			startActivity(new Intent(this, Help.class));
            
	    
	    
//	    case R.id.action_settings:
//	        Toast.makeText(this, "Menu Item 1 selected", Toast.LENGTH_SHORT)
//	            .show();
	        break;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	    return true;
	}








	private void showHelp() {
		// TODO Auto-generated method stub
		
	}
	protected void calculateTotal() {
		// TODO Auto-generated method stub
		try {
		
//			int price = GlobalVars.getSeekBarPrice();
//			float discount = GlobalVars.getSeekBarDiscount();
		// USING DOUBLE TO CONVERT TO PERCENTAGE HERE RESULTED IN WRONG RESULTS FOR SOME REASON.
//			double discountPercent = discount * .01;
//			double total = price - (price * discount);
			
			
		// PARSING DOUBLES REQUIRED ME TO REMOVE MY $ AND % SIGNS SO THAT DIDN'T WORK	
SharedPreferences MYPREFS = PreferenceManager.getDefaultSharedPreferences(this);
			
			String tax = MYPREFS.getString("tax", "");
			
			if(tax == "null" || tax.isEmpty()) {
				testPrefLoad.setVisibility(View.INVISIBLE);
			    
				  tax = "0";
				  
				} else {
					
					
					testPrefLoad.setVisibility(View.VISIBLE);
				}
			
		double taxPercent = Double.parseDouble(tax);
		double taxAmount = taxPercent / 100;
		double price = Double.parseDouble(priceEditText.getText().toString());
		double discount = Double.parseDouble(discountEditText.getText().toString());
		double discountPercentage = discount / 100;
		double discount2 = Double.parseDouble(discountEditText2.getText().toString());
		double discountPercentage2 = discount2 / 100;
		double total = price - (price * discountPercentage);
		double total2 = total - (total * discountPercentage2);
		double saved = price - total2;
		double taxes = total2 * taxAmount;
		double total3 = total2 + taxes;
		DecimalFormat form = new DecimalFormat(getString(R.string.dollar_sign) +"0.00");
		totalTextView.setText(form.format(total3));
		testPrefLoad.setText("Discounted Price includes " + form.format(taxes) + " in taxes.");
		savedTextView.setText("You Saved " + form.format(saved));
		
			
		// THIS WORKED BUT RESULTED IN REALLY LONG NUMBERS SOMETIMES. NO FORMATTING DECIMAL PLACES	
//		totalTextView.setText("$" + total);

	    /// Your code
			} catch(NumberFormatException nfe) {
			   Log.e("MyApp", nfe.getMessage(), nfe);
			}
		
	

priceEditText.addTextChangedListener(new TextWatcher() {

	@Override
	public void afterTextChanged(Editable p) {
		// TODO Auto-generated method stub
		
		try{
            //Update Seekbar value after entering a number
			
            priceSeekBar.setProgress(Integer.parseInt(p.toString()));
        } catch(Exception ex) {}
		
		
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// the code below moves the cursor to the end as you type
		priceEditText.setSelection(priceEditText.getText().length());
//		GlobalVars.setSeekBarPrice(arg1);
		
		
		
		
	}
	
	
	
	
	
	
});

discountEditText.addTextChangedListener(new TextWatcher() {

	@Override
	public void afterTextChanged(Editable p) {
		// TODO Auto-generated method stub
		
		try{
            //Update Seekbar value after entering a number
			
            discountSeekBar.setProgress(Integer.parseInt(p.toString()));
        } catch(Exception ex) {}
		
		
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		discountEditText.setSelection(discountEditText.getText().length());
//		GlobalVars.setSeekBarPrice(arg1);
		
		
		
		
	}
	
	
	
	
	
	





});

discountEditText2.addTextChangedListener(new TextWatcher() {

	@Override
	public void afterTextChanged(Editable p) {
		// TODO Auto-generated method stub
		
		try{
            //Update Seekbar value after entering a number
			
            discountSeekBar2.setProgress(Integer.parseInt(p.toString()));
        } catch(Exception ex) {}
		
		
	}

	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		discountEditText2.setSelection(discountEditText2.getText().length());
//		GlobalVars.setSeekBarPrice(arg1);
		
		
		
		
	}
	


	
	
	

});}









	protected void openPrefs() {
		// TODO Auto-generated method stub
		Intent startNewActivityOpen = new Intent(MainActivity.this, Prefs.class);
		startActivityForResult(startNewActivityOpen, 0);
	}}











	
	
	
	

	
	

	






