package com.eeculink.discountcalculator;



import java.util.ArrayList;

import com.eeculink.discountcalculator.util.IabHelper;
import com.eeculink.discountcalculator.util.IabResult;
import com.eeculink.discountcalculator.util.Inventory;
import com.eeculink.discountcalculator.util.Purchase;
import com.google.analytics.tracking.android.EasyTracker;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

public class Prefs extends PreferenceActivity implements OnSharedPreferenceChangeListener{
	
	EditTextPreference pref_maxprice;
	EditTextPreference pref_tax;
	private static final String maxPrice = "maxPrice";
	private static final String tax = "tax";
	Preference purchase;
//	private static final String TAG = "inappbilling";
//	IabHelper mHelper;
//	static final String ITEM_SKU = "android.test.purchased";
	PreferenceScreen myPreferenceScreen;
//	boolean mIsPremium = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState); 
		getActionBar().setDisplayHomeAsUpEnabled(true);
		SharedPreferences MYPREFS = getPreferences(0);
		String maxPrice = MYPREFS.getString("pref_maxprice", "100");
		String tax = MYPREFS.getString("pref_tax", "0");
		
		
		
		addPreferencesFromResource(R.xml.preferences);
		EditTextPreference pref_maxprice = (EditTextPreference)       findPreference("pref_maxprice");
		EditTextPreference pref_tax = (EditTextPreference) findPreference("pref_tax");
//		ArrayList skuList = new ArrayList();
//		skuList.add("remove_ads");
//		Bundle querySkus = new Bundle();
//		querySkus.putStringArrayList(this, skuList);
//
//		Bundle skuDetails = mService.getSkuDetails(3, 
//				   getPackageName(), "inapp", querySkus);
		
//		BILLING CODE
//		String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhZ1gZQJY/FP9QWrV2UmkL+gXNbPD36nD17k4Oe/Nxi2z6muXVPT013fYLSJtsHofVqLUzdu9a7njS6Ezd4uwuIG16hXLsQhLT2UrPRiYdbtTC4dK+RglG1ZNbuLC1OUTBAtk5UtLcuCpBVxs9zUSdJaUE0cauy6IndG5sLlU1/9SIrmmcHLIpDvGQ5Mva+CcsvbwOiheGjmOKcSSRsLnI78ZYbkcCtUlyHRDHHvlO7gOTP7bapPQdVJKSA6JwgKpN48xN53MCILBpVg9fON2qv5dPetFUVG44r+FXnuQd0oKK1H7TyEzijuxbNG/HSutHz7aOjLlZF0XsIatE2bjgwIDAQAB";
//		mHelper = new IabHelper(this, base64EncodedPublicKey);
//		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
//			
//			@Override
//			public void onIabSetupFinished(IabResult result) {
//				if (!result.isSuccess()) {
 //    	           Log.d(TAG, "In-app Billing setup failed: " + 
//					result);
				
//			}	else {             
//	      	    Log.d(TAG, "In-app Billing is set up OK");
	      	  
//      }
//		}});

		
		
		
		
		
		
		
		
//	This is how to respond to clicks to the preference items		
		getPreferenceManager().findPreference("pref_share").setOnPreferenceClickListener(new OnPreferenceClickListener()
	
	{   
        Intent myIntent = new Intent(getApplicationContext(), Prefs.class);
        public boolean onPreferenceClick(Preference preference)
        {	Intent i=new Intent(android.content.Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(android.content.Intent.EXTRA_SUBJECT,"You need to try this free app I found!");
        i.putExtra(android.content.Intent.EXTRA_TEXT, "I found a great free app you should try. It's called Quick Discount and it's awesome. Get it here: http://goo.gl/aqkftb");
        startActivity(Intent.createChooser(i,"Share via"));
        	//this is where I need to add the code for what happens when the PURCHASE button is clicked.
//        	buyClick();
//            startService(myIntent);
			return true;
        }
        
        
        
        
        
    });
		getPreferenceManager().findPreference("pref_rate").setOnPreferenceClickListener(new OnPreferenceClickListener()
		
		{

			@Override
			public boolean onPreferenceClick(Preference preference) {
				// TODO Auto-generated method stub
				
				
				Uri uri = Uri.parse("market://details?id=" + "com.eeculink.discountcalculator");
				Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
				try {
				  startActivity(goToMarket);
				} catch (ActivityNotFoundException e) {
				  Toast.makeText(getApplicationContext(), "Couldn't launch the market", Toast.LENGTH_LONG).show();
				};
				return false;
			}   
	        
	        });
	        
	        
	        
	        
	        
	    }
				
				
				
				
;
		
	
//	BILLING CODE	
//	public void buyClick() {
		// TODO Auto-generated method stub
		
//		Log.v("purchase clicked", "purchase button clicked");
//		mHelper.launchPurchaseFlow(this, ITEM_SKU, 10001,   
//  			   mPurchaseFinishedListener, "");
		
//	}
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, 
//	     Intent data) 
//	{
//	      if (!mHelper.handleActivityResult(requestCode, 
//	              resultCode, data)) {     
//	    	super.onActivityResult(requestCode, resultCode, data);
//	      }
//	}

//	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		
//		@Override
//		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
//			// TODO Auto-generated method stub
//			
//			if (result.isFailure()) {
//				Toast.makeText(getApplicationContext(), "Something went wrong with your purchase",
//			    		   Toast.LENGTH_LONG).show();
//				consumeItem();
//			      return;
//			      
//			 }      
//			 else if (purchase.getSku().equals(ITEM_SKU)) {
//			     
//				 consumeItem();
//				 
//			     Toast.makeText(getApplicationContext(), "Thank you. Purchase Successful.",
//			    		   Toast.LENGTH_LONG).show();
//			     
			     
			    
			     
//			}
			
			
//		}
//		protected void consumeItem() {
			
			
//			IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
				
//				@Override
//				public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
//					if (result.isFailure()) {
//						  // Handle failure
//					      } else {
//				                 mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU), 
//							mConsumeFinishedListener);
//				                 mIsPremium = inventory.hasPurchase(ITEM_SKU);
//				                 Log.d(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));
//					      }
					
//				}
//			};
//	};
	
//	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		
//		@Override
//		public void onConsumeFinished(Purchase purchase, IabResult result) {
			// TODO Auto-generated method stub
//			if (result.isSuccess()) {		    	 
//			   	  clickButton.setEnabled(true);
//				Toast.makeText(getApplicationContext(), "item succesfully consumed",
//			    		   Toast.LENGTH_LONG).show();
//			 } else {
//				 Toast.makeText(getApplicationContext(), "item not consumed",
//						 Toast.LENGTH_LONG).show();
//			 }
//		}
//	};	
//		
//	};
	
@Override
public void onStart() {
  super.onStart();
 
  EasyTracker.getInstance(this).activityStart(this);  // Google Analytics.
}

	
	@Override
    protected void onStop(){
       super.onStop();

       EasyTracker.getInstance(this).activityStop(this);  // Google Analytics

    }


	
	@Override
    protected void onResume(){
        super.onResume();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
        .registerOnSharedPreferenceChangeListener(this);
    }
 
    @Override
    protected void onPause() {
        super.onPause();
        // Unregister the listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
        .unregisterOnSharedPreferenceChangeListener(this);
    }

    
    

    
    
    public void savePrefs(String key, String value) {
     	SharedPreferences MYPREFS = PreferenceManager.getDefaultSharedPreferences(this);
    	Editor edit = MYPREFS.edit();
    	edit.putString(key, value);
    	edit.commit();
    	Log.v("saving_error", "Did not save");
    	
    	
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
            String key){
		// TODO Auto-generated method stub
		Log.v("preferencechangetriggered", "preferencechange");
		
		updatePreference(key);
		
		
	}

	private void updatePreference(String key) {
		// TODO Auto-generated method stub
		
		SharedPreferences MYPREFS = PreferenceManager.getDefaultSharedPreferences(this);
    	Editor edit = MYPREFS.edit();
    	Log.v("saving_error", "before put");
	    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
	    String pref_maxpriceString = preferences.getString("pref_maxprice", "");
	    String pref_taxString = preferences.getString("pref_tax", "");
    	edit.putString(maxPrice, pref_maxpriceString);
    	edit.putString(tax, pref_taxString);
    	edit.commit();
    	
     	Log.v("saving_error", "after put");
	}
 









}


